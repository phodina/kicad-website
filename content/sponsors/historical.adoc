+++
title = "Historical Sponsors"
date = "2021-04-19"
categories = [ "Sponsors" ]
summary = "Previous sponsors who have helped KiCad's development"
[menu.main]
    parent = "Sponsors"
    name   = "Historical Sponsors"
+++

KiCad would not be where it is today without the previous sponsorship from many individuals and corporations.

We encourage corporate users of KiCad to support the community by link:{{% ref path="become-a-sponsor.adoc" %}}[joining our Corporate Sponsorship Program].


{{< sponsors_list title="" filter="historical">}}

