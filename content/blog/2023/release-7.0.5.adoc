+++
title = "KiCad 7.0.5 Release"
date = "2023-05-28"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the third series 7 bug fix release.
The 7.0.5 stable version contains critical bug fixes and other minor improvements
since the previous release.  Due to a last minute critical bugs which affected
7.0.3 and 7.0.4 packages, there was no official 7.0.3 or 7.0.4 releases.

<!--more-->

A list of all of the fixed issues since the 7.0.2 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/25[KiCad 7.0.3
milestone] and https://gitlab.com/groups/kicad/-/milestones/26[KiCad 7.0.4
milestone] and https://gitlab.com/groups/kicad/-/milestones/27[KiCad 7.0.5
milestone] pages. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 7.0.5 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/7.0/[7.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- https://gitlab.com/kicad/code/kicad/-/commit/4c129ec2088fe6565239af765661fff021c6004a[Initialize CURL library for thread safety].
- https://gitlab.com/kicad/code/kicad/-/commit/54847967458f5fc1793af71109245a1386fc50ab[Fix bitmap rotation and mirroring issues].
- Do not close dialogs on escape key when typing in a text control. https://gitlab.com/kicad/code/kicad/-/issues/14514[#14514]
- Do not display additional space before subscript/superscript text in text boxes. https://gitlab.com/kicad/code/kicad/-/issues/14683[#14683]
- Make control-backspace clear until last whitespace in text edit controls. https://gitlab.com/kicad/code/kicad/-/issues/13910[#13910]
- Catch unhandled exception at startup. https://gitlab.com/kicad/code/kicad/-/issues/14703[#14703]
- Use correct alpha blending when plotting to PDF. https://gitlab.com/kicad/code/kicad/-/issues/14238[#14238]
- Fix broken bug report URL. https://gitlab.com/kicad/code/kicad/-/issues/14759[#14759]

=== Schematic Editor
- Do not collapse tree when filtering symbol chooser dialog search results. https://gitlab.com/kicad/code/kicad/-/issues/13905[#13905]
- Do not ignore "Exclude from BOM" and "Exclude from board" attributes for derived symbols. https://gitlab.com/kicad/code/kicad/-/issues/13740[#13740]
- Correctly handle symbols with '/' character in the name. https://gitlab.com/kicad/code/kicad/-/issues/14057[#14057]
- Fix crash when pasting then pressing escape. https://gitlab.com/kicad/code/kicad/-/issues/14315[#14315]
- Ignore empty field name templates. https://gitlab.com/kicad/code/kicad/-/issues/14552[#14552]
- https://gitlab.com/kicad/code/kicad/-/commit/bdca18cc9f71985774527d4e4e09d0c5299c7d22[Ensure item is on grid if there was no motion between clicks].
- Fix clipped text in ERC dialog. https://gitlab.com/kicad/code/kicad/-/issues/14569[#14569]
- Save worksheet file name in project file. https://gitlab.com/kicad/code/kicad/-/issues/14219[#14219]
- Allow hex entry of 0 transparency in color picker. https://gitlab.com/kicad/code/kicad/-/issues/14646[#14646]
- Fix datasheet path resolution when path contains double slashes. https://gitlab.com/kicad/code/kicad/-/issues/14564[#14564]
- Set focus to text control when editing text box properties. https://gitlab.com/kicad/code/kicad/-/issues/14685[#14685]
- Fix for mouse stuck in canvas issue. https://gitlab.com/kicad/code/kicad/-/issues/12653[#12653]
- Handle user defined attributes when importing Eagle schematics. https://gitlab.com/kicad/code/kicad/-/issues/13798[#13798]
- Fix broken nets using hierarchical labels. https://gitlab.com/kicad/code/kicad/-/issues/14657[#14657]
- Remove extra blank line in schematic file when saving with ERC markers. https://gitlab.com/kicad/code/kicad/-/issues/11405[#11405]
- Fix crash when entering subsheets with resolved variables. https://gitlab.com/kicad/code/kicad/-/issues/14720[#14720]
- Mirror netclass labels correctly. https://gitlab.com/kicad/code/kicad/-/issues/14758[#14758]

=== Spice Simulator
- Do not show both sets of pins for symbols with alternate body styles (DeMorgan). https://gitlab.com/kicad/code/kicad/-/issues/14230[#14230]
- Do not create SPICE netlists with spaces in net names. https://gitlab.com/kicad/code/kicad/-/issues/14724[#14724]

=== Symbol Editor
- Save library when it's selected in library tree panel. https://gitlab.com/kicad/code/kicad/-/issues/14220[#14220]
- Cmd+A (Select All) selects all symbol editor components instead text in active text input box. https://gitlab.com/kicad/code/kicad/-/issues/14237[#14237]
- Fix crash when infobar outlives the current symbol. https://gitlab.com/kicad/code/kicad/-/issues/14680[#14680]

=== Board Editor
- Highlight drilled hole to hole clearance of same nets in router. https://gitlab.com/kicad/code/kicad/-/issues/12781[#12781]
- Fix graphical glitch after DRC and subsequent undo action. https://gitlab.com/kicad/code/kicad/-/issues/13446[#13446]
- Do not detect DRC error when the footprint was deleted or updated with new one. https://gitlab.com/kicad/code/kicad/-/issues/14351[#14351]
- Allow setting board minimum widths to zero. https://gitlab.com/kicad/code/kicad/-/issues/13822[#13822]
- Fix crash when differential pair routing. https://gitlab.com/kicad/code/kicad/-/issues/14537[#14537]
- Fix invalid overbar text for escaped characters. https://gitlab.com/kicad/code/kicad/-/issues/14553[#14553]
- Fix overbar when using subscript characters. https://gitlab.com/kicad/code/kicad/-/issues/13449[#13449]
- Trim VRML export layers to board outline. https://gitlab.com/kicad/code/kicad/-/issues/14557[#14557]
- Handle multiple holes in footprints. https://gitlab.com/kicad/code/kicad/-/issues/14468[#14468]
- Do not treat via placement as DRC violation when routing. https://gitlab.com/kicad/code/kicad/-/issues/14603[#14603]
- Record parent rule for DRC assertions. https://forum.kicad.info/t/a-few-questions-about-custom-rules-syntax/40068/7[See forum discussion]
- Fix incorrect mounting hole size on DSN export. https://gitlab.com/kicad/code/kicad/-/issues/14656[#14656]
- Allow double-clicking item to open properties dialog in search pane. https://gitlab.com/kicad/code/kicad/-/issues/12475[#12475]
- Fix crash when loading a file having vias on connected layers only. https://gitlab.com/kicad/code/kicad/-/issues/14668[#14668]
- Add missing property to footprint texts table. https://gitlab.com/kicad/code/kicad/-/issues/14357[#14357]
- Fix copper sliver issues. https://gitlab.com/kicad/code/kicad/-/issues/14549[#14549]
- Fix issue routing differential pairs to footprint pads. https://gitlab.com/kicad/code/kicad/-/issues/14324[#14324]
- Expand solder mask for NPTH pads. https://gitlab.com/kicad/code/kicad/-/issues/14693[#14693]
- Fix field automatic placement issue. https://gitlab.com/kicad/code/kicad/-/issues/14127[#14127]
- Don't allow thickness of 0 in STEP export in case of an empty stackup. https://gitlab.com/kicad/code/kicad/-/issues/10790[#10790]
- Make footprint drag include connected traces. https://gitlab.com/kicad/code/kicad/-/issues/14515[#14515]
- Add missing property to footprint texts table. https://gitlab.com/kicad/code/kicad/-/issues/14357[#14357]
- Use constraints to determine netclass width. https://gitlab.com/kicad/code/kicad/-/issues/14190[#14190]
- Fix rotating bitmaps. https://gitlab.com/kicad/code/kicad/-/issues/14197[#14197]
- Fix shove router clearance DRC violations. https://gitlab.com/kicad/code/kicad/-/issues/14707[#14707]
- Do not route trans with clearance errors. https://gitlab.com/kicad/code/kicad/-/issues/14659[#14659]
- Fill shapes with open path when importing SVG. https://gitlab.com/kicad/code/kicad/-/issues/14518[#14518]
- Ensure track posture switches on first attempt. https://gitlab.com/kicad/code/kicad/-/issues/12369[#12369]
- Prevent unwanted minus sign in properties panel. https://gitlab.com/kicad/code/kicad/-/issues/14168[#14168]
- Adjust stroke font output to match version 6 output. https://gitlab.com/kicad/code/kicad/-/issues/14609[$14609]
- Fix crash on start of routing. https://gitlab.com/kicad/code/kicad/-/issues/14733[#14733]
- Fix crash when shoving via or trace. https://gitlab.com/kicad/code/kicad/-/issues/14741[#14741]
- Fix broken router collision with self behavior. https://gitlab.com/kicad/code/kicad/-/issues/14795[#14795]

=== Footprint Editor
- Crash when importing version 5.1 settings. https://gitlab.com/kicad/code/kicad/-/issues/14691[#14691]

=== Gerber Viewer
- Fix rounded corners when exporting to KiCad board file. https://gitlab.com/kicad/code/kicad/-/issues/14136[#14136]

=== 3D Viewer
- Use correct board stack up layer color settings. https://gitlab.com/kicad/code/kicad/-/issues/14616[#14616]
- Show footprint board regardless of project. https://gitlab.com/kicad/code/kicad/-/issues/14246[$14246]

=== PCB Calculator
- https://gitlab.com/kicad/code/kicad/-/commit/6d93fa046cefabdcb3c7fc3d3395074ccead987a[Save regulator calculator settings].

=== Drawing Sheet Editor
- Add missing grid properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/14612[#14612]

=== Command Line Interface
- Load custom drawing sheets for board. https://gitlab.com/kicad/code/kicad/-/issues/14171[#14171]
- Do not fail silently if output directory does not exist. https://gitlab.com/kicad/code/kicad/-/issues/14437[#14437]

=== Windows
- Fix broken text selection for some UI elements. https://gitlab.com/kicad/code/kicad/-/issues/14692[#14692]
